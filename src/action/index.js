import { STATE, SUBJECT, INPUT } from '../type/index';

const loadState = () => ({
    type: STATE.STATE_LOAD
})

const successState = (state) => ({
    type: STATE.STATE_SUCCESS,
    state
})

const failState = () => ({
    type: STATE.STATE_FAIL
})

const loadSubject = () => ({
    type: SUBJECT.SUBJECT_LOAD
})

const successSubject = (subject) => ({
    type: SUBJECT.SUBJECT_SUCCESS,
    subject
})

const failSubject = () => ({
    type: SUBJECT.SUBJECT_FAIL
})

const fetchInput=(input)=>({
    type:INPUT.FETCH_INPUT,
    input
})

export {
    loadState,
    successState,
    failState,
    successSubject,
    fetchInput
}