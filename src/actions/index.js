import { SEE_MORE, FETCH_INPUT, GET_STATE, GET_SUBJECT, FIND_TRACK,SCHOOL_LOAD ,ANNOUNCEMENT} from './type';

const actionSeeMore = (id) => ({
    type: SEE_MORE,
    id
})

const actionState = (state) => ({
    type: GET_STATE,
    state
})

const actionSubject = (subject) => ({
    type: GET_SUBJECT,
    subject
})

const actionInput = (input) => ({
    type: FETCH_INPUT,
    input
})

const actionFind = (track) => ({
    type: FIND_TRACK,
    track
})

const actionAnouncement = (announcement) => ({
    type: ANNOUNCEMENT,
    announcement
})

const loadSchools=(shool)=>({
    type:SCHOOL_LOAD,
    payload:shool
})

export {
    actionState,
    actionSubject,
    actionInput,
    actionFind,
    actionSeeMore,
    loadSchools,
    actionAnouncement
}