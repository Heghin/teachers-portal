import {actionState,actionSubject,actionInput,actionAnouncement,loadSchools,actionSeeMore} from '../actions/index'

const SERVER_LINK = "http://admin.teacher.am/api/"
//const SERVER_LINK = "http://192.168.0.114:9000/api/"
let token = ""

const getState=()=>dispatch=>{
    fetch(SERVER_LINK+"region/?limit=1000")
    .then((res)=>res.json())
    .then(state=>dispatch(actionState(state.results)))
}

const getAnnouncement=()=>dispatch=>{
    fetch(SERVER_LINK+"announcement/?limit=1000")
    .then((res)=>res.json())
    .then((res)=> dispatch(actionAnouncement(res)))
}
const getAnnouncementOnly=()=>{
    return fetch(SERVER_LINK+"announcement/")
    .then((res)=>res.json())
    
}

const getAnnouncementByID=(id)=>{
return fetch(SERVER_LINK+"announcement/"+id+"/")
    .then((res)=>res.json())
}

const getSubject=()=>dispatch=>{
    fetch(SERVER_LINK+"subject/?limit=1000")
    .then((res)=>res.json())
    .then(subject=>dispatch(actionSubject(subject.results)))
}
const getSchools =()=>(dispatch)=>{
    fetch(SERVER_LINK+"school/?limit=1000")
    .then((res)=>res.json())
    .then(schools=>dispatch(loadSchools(schools.results)))
}

const fetchInput=(inputData)=>dispatch=>{
    fetch(SERVER_LINK+"teacher/",{
        method:'POST',
        headers:{
            "content-type":"application/json"
        },
        body:JSON.stringify(inputData)
    })
    .then(res=>res.json())
    .then(input=>dispatch(actionInput(input))
    );
 }

 const saveTeacher = (inputData,id)=>{
    return fetch(SERVER_LINK+"teachers/"+id+"/",{
        method:'PUT',
        headers:{
            "content-type":"application/json"
        },
        body:JSON.stringify(inputData)
    })
    .then(res=>res.json())
 }

 const getMy = ()=>{
    console.log(token);
    return fetch(SERVER_LINK+"me/",{
        method:"GET",
        headers:{
            'Authorization': 'Bearer ' + token,
        }
    }).then(res=>{
        return res.json()
    })    
 }
 const getTeacherInfo = (id)=>{
    return fetch(SERVER_LINK+"teacher/"+id,{
        method:"GET",
        headers:{
            'Authorization': 'Bearer ' + token,
        }
    }).then(res=>{
        return res.json()
    })   
 }
 const login=( username,password)=>{
     return fetch(SERVER_LINK.substr(0,SERVER_LINK.length-4)+"api-token-auth/",{
         method:"POST",
         headers:{
            "content-type":"application/json"
        },
        body:JSON.stringify({username,password})
     })
     .then(res=>res.json()).then(data=>{
         token = data.token
         return token
     })
 }

 const searchShow=()=>(dispatch)=>dispatch({ type: 'SHOW', payload: 'true' });
 const seeMore=(id)=>(dispatch)=>dispatch(actionSeeMore(id));

 export {getState, login, saveTeacher,getSubject,fetchInput,getMy,getTeacherInfo, getSchools,getAnnouncement,searchShow,seeMore,getAnnouncementByID,getAnnouncementOnly}