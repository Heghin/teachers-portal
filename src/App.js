import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import Header from './components/header/header';
import Footer from './components/footer/footer';
import Main from './components/main/main'
import Statement from './components/statement/statement';
import TextArea from './components/textarea/textarea';
import TeacherAccount from './components/teacherAccount/teacherAccount';
import Contacts from './components/contact_with_us/contact_with_us';
import Registration from './components/registration/registration'

import './App.css';
import contact_with_us from './components/contact_with_us/contact_with_us';

const token = "account_token"

class App extends Component {
  constructor() {
    super();
    this.state = {
      subjects: ["Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա",
        "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա",
        "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա",
        "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա", "Մաթեմատիկա"

      ],
      community: ["համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1",
        "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1",
        "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1", "համայնք-1"
      ]
    }
  }
  render() {
    return (
      <Router>

        <div className="Container">

          <Header />
          <Switch>
            <Route path="/about_us" component={Contacts} />
            <Route path="/statement/:id" component={TextArea} />
            <Route path="/statement" component={Statement} />
            <Route path="/account/:token" component={TeacherAccount} />
            <Route path="/" component={Main} />
            
          </Switch>

          {this.props.state.rshow ? <Registration RegistrationShow={() => this.props.dispatch({ type: "R_SHOW", payload: false })} /> : null}
          {this.props.state.rshow ? <div className='pop-up' onClick={() => this.props.dispatch({ type: "R_SHOW", payload: false })}></div> : null}
        
        </div>
      </Router>
    );
  }
}

export default connect(
  state => ({
    state: state.mainReducer
  })
)(App);
