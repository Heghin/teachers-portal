import {STATE} from '../../type/index';

const stateSuccessReducer=(state=[], action)=>{
    switch(action.type){
        case STATE.STATE_SUCCESS:
        return [
            ...state,
            ...action.state
        ]
    }
    return state;
}
export default stateSuccessReducer;