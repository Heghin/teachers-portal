const maincont={
        navbaritem1:"Նախագծի մասին",
        navbaritem2:"Ինչպես գրանցվել",
        navbaritem3:"Հետադարձ կապ",
        spaceitem1:'Շիրակի',
        spaceitem2:'մարզ',
        button:'Գրանցվել',
        save:"Պահել",
        confirmed:"Հաստատել",
        statement_button:'Հայտարարություններ',
        main:'Գլխավոր',
        search:'Որոնում',
        statement:"Հայտարարություն",
        heading:'Թափուր աշխատատեղեր Շիրակի մարզի դպրոցներում',
        see_more:'Տեսնել ավելին',
        school_name:"Դպրոցի անվանումը՝ ",
        subject:"Առարկան",
        hour:"Ժամաքանակը",
        during_competition:"Դիմումները ընդունվում են մինչ`",
        request_deadline:"Մրցույթի անցկացման մեկնարկը՝",
        text:'Ուսուցիչ պարոններ ու քույրեր, զգուշությամբ և երկյուղածությամբ մոտեցեք դաստիարակության գործին. խիստ փափուկ պաշտոն մըն է ձերը։ Դաստիարակելու կոչված եք սերունդ մը, որ ապագա ազգն է։ Սխալ ուղղությամբ՝ ազգ մը կը խորտակեք վերջը:',
        section_about_heading:'Ինչպես գրանցվել',
        text_bout:'Lorem Ipsum is simply dumnder took a galleses, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock,the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum,',
        activ_statement:'Ակտիվ Հայտարարություններ',
        link1:'Կապ մեզ հետ',
        link2:'Ինչպես գրանցվել',
        link3:'Ուսուցիչներ',
        registration_name:'Անուն',
        registration_surname:'Ազգանուն',
        registration_university:'Բուհ',
        registration_profession:'Մասնագիտություն', 
        registration_qualification:'Որակավորում',  
        registration_phone:'Հեռախոսահամար', 
        registration_email:'էլեկտրոնային հասցե', 
        work_orientation_item1:'Վարչական',
        work_orientation_item2:'Ուսումնական',
        preferred_subjects:'Նախընտրելի հաստիքները', 
        community_choice:'Համայնքի ընտրություն',
        letter:"Նամակ Ռուսաց թագավորին",
        consent:'համաձայն եմ, որ ինձ ենթարկեն աշխատանքային թրաֆիքինգի',
        send:'Ուղարկել',
        rshow:false
}
export default (state=maincont,actions)=>{

    if(actions.type=="R_SHOW"){
        
        return {...state, rshow:actions.payload}
    }
    return state

}