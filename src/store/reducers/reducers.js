import { combineReducers } from 'redux';
import mainReducer from './main-reducer/main-reducer';
import showReducer from './show-reducer/show-reducer';
import subjectReducer from './subject-reducer/index';
import stateReducer from './state-reducer/index';
import findReducer from './find-reducer/index'
import schools from "./schools-reducer";
import announcementReducer from './announcement-reducer'

export default combineReducers(
    {
        mainReducer,
        showReducer,
        subject:subjectReducer,
        state:stateReducer,
        findTrack:findReducer,
        schools,
        announcement:announcementReducer
    }
)