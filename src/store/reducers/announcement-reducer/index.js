import {ANNOUNCEMENT} from '../../../actions/type'

const announcementReducer=(state=[],action)=>{
    switch(action.type){
        case ANNOUNCEMENT:
        return action.announcement   
    }
    return state
}

export default announcementReducer;