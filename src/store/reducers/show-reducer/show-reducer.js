export default(state={show:false},action)=>{
    switch(action.type){
        case 'SHOW':
        return {...state,show:action.payload}
    }
    return state
}