import {SELECT_ITEM} from '../../type/index';

const selectItemReducer=(state=[], action)=>{
    switch(action.type){
        case SELECT_ITEM.SELECT_ITEM:
        let tmp=[...state]
        tmp.push(action.payload)
        return tmp
    }
    return state;
}
export default selectItemReducer;