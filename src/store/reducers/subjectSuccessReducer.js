import {SUBJECT} from '../../type/index';

const subjectSuccessReducer=(state=[], action)=>{
    switch(action.type){
        case SUBJECT.SUBJECT_SUCCESS:
        return [
            ...state,
            ...action.subject
        ]
    }
    return state;
}
export default subjectSuccessReducer;