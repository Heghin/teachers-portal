import {GET_STATE} from '../../../actions/type'

const stateReducer=(state=[], action)=>{
    switch(action.type){
        case GET_STATE:
        return action.state
    }
    return state;
}

export default stateReducer;