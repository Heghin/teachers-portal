import {FETCH_INPUT} from '../../../actions/type';

initialState={
    item:{}
}

export default function(state=initialState,action){
    switch(action.type){
        case FETCH_INPUT:
        return{
            ...state,
            item:action.input
        }
        default:
        return state ; 
    }
}