import { SCHOOL_LOAD } from "../../../actions/type";

const schoolsReducer=(state=[], action)=>{
    switch(action.type){
        case SCHOOL_LOAD:
        return action.payload
    }
    return state;
}

export default schoolsReducer;