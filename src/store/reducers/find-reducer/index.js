import {FIND_TRACK} from '../../../actions/type'

const findReducer=(state='',action)=>{
    switch(action.type){
        case FIND_TRACK:
        return action.track
    }
    return state
}

export default findReducer;