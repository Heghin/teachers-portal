import {GET_SUBJECT} from '../../../actions/type'

const subjectReducer=(state=[], action)=>{
    switch(action.type){
        case GET_SUBJECT:
        return action.subject
    }
    return state;
}

export default subjectReducer;