import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {actionFind} from '../../actions/index'
import arrow1 from "../../assets/img/Path 3.svg";
import search from '../../assets/img/Search.svg'

class SearchLine extends Component {
    constructor(props){
        super(props);
        this.state={
            show:true
        }
    }
    find
    searchFunction(){
        console.log("search",this.find.value);
        
    }
    render() {
       // console.log(this.find.value);
        
        return (
            <div className='Statement-search-line'>
                <Link className='statement-button-link' to="/" >
                    <img className='arrow' src={arrow1} />
                    <span>{this.props.state.main}</span>
                </Link>
                <div className='search'>
                    <input type="text" name="statement_search" 
                    ref={(track)=>{ this.find=track}}
                    className={(this.state.show)?'input-search':'input-search input-search-width' } 
                    placeholder={this.props.state.search} 
                    onClick={()=>this.props.findTrack(this.find.value)}
                    />
                    <div className='search-icon' 
                    onClick={()=>{
                        this.setState({
                            show:false
                        })
                        this.props.findTrack(this.find.value)
                        }}>
                        <img src={search} /></div>
                </div>
            </div>
        );
    }
}
const mapDispatchToProps=(dispatch)=>({
    
    findTrack:(track)=>dispatch(actionFind(track))
})
export default connect(
    (state) => ({ state: state.mainReducer }),
    mapDispatchToProps
)(SearchLine);