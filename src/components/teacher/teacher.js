import React, { Component } from 'react';
import { connect } from 'react-redux';
import "react-alice-carousel/lib/alice-carousel.css"
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import AliceCarousel from 'react-alice-carousel';
import TeacherItem from '../teacher-item/teacher-item';
import arrow1 from "../../assets/img/Path 1.svg";
import arrow2 from "../../assets/img/Path 3.svg";
import {getAnnouncementOnly} from "./../../api"



class Teacher extends Component {
    responsive = {
        0: { items: 1 },
        720: { items: 2 },
        1200:{items:3}
      }
    constructor(props) {
        super(props);
        this.state={
            teacherImages:[
                "https://www.ewa.org/sites/main/files/imagecache/medium/main-images/bigstock-pretty-teacher-smiling-at-came-69887626.jpg",
                "https://nvdaily.ru/wp-content/uploads/2018/08/screen-shot-2017-03-28-at-3-46-31-pm_orig.png",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSeh-MUog4oOYMyxdFyUXJTpL_wE3FawzUVqPQWEIeuWpmGDt8j",
                "https://sakhalife.ru/wp-content/uploads/2018/10/how_to_become_a_teacher_612px.jpg",
                "https://shatsk.rayon.in.ua/upload/news/6/2019-01/154843129146/t_1_360b74d-teacher.jpg"
            ],
            items:[]
        }

    }

    componentDidMount(){
        getAnnouncementOnly().then(data=>{
            console.log(data);
            
                this.setState({items:data})
        })
    }
    render() {
        
        let store = this.props.state
        return (
                <div className="sectionteacher" id="teacher">
                    <div className='section-heading'>
                        <span className='heading'>{store.activ_statement}</span>
                        <div className='section-line'></div>
                    </div>
                    <div className='section-teacher'>
                        <div className='teacher-yellow-left'>
                            <div className='line-arrow'>
                                {/* <img className='arrow' src={arrow2}  />
                                <div className='line'></div>
                                <img className='arrow' src={arrow1} /> */}
                            </div>
                        </div>
                        <div className='teacher-yellow-right'>
                        </div>
                        <div className='button-line'>
                            <button className="button-slide "><Link className='link-seeMore' to="/statement">{this.props.state.see_more}</Link></button>
                        </div>
                        <div className='teacher-cont'>

                        <AliceCarousel
                            items={this.state.items.map((item, index) => { return<Link to={`/statement/${item.id}`}><TeacherItem data={item} key={index}/></Link> })}
                            responsive={this.responsive}
                            autoPlayInterval={2000}
                            buttonsDisabled={true}
                            autoPlayDirection="rtl"
                            autoPlay={true}
                            fadeOutAnimation={true}
                         //   mouseDragEnabled={true}
                         ///   playButtonEnabled={true}
                          //  disableAutoPlayOnAction={true}
                           // onSlideChange={this.onSlideChange}
                           // onSlideChanged={this.onSlideChanged}
                        />                       
                        </div>
                    </div>
                </div>
        );
    }
}
export default connect(
    (state) => ({ state: state.mainReducer })
)(Teacher);