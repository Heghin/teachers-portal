import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { getAnnouncementByID } from '../../api';
import { connect } from 'react-redux';
import moment from 'moment'
import 'moment/locale/hy-am';


class TeacherItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }

    //{moment(this.state.data.request_deadline).lang("hy-am").format('LLLL')}  request_deadline
    render() {

        return (
            <Router>
                <div className="teacher-item"  >
                    <div className='school-name'>{this.props.data.school_name}</div>
                    <div className='subject-name'>{this.props.data.subject_name}</div>

                    <div className="teacher-item-date">
                        <hr />
                        {moment(this.props.data.request_deadline).lang("hy-am").format('LLLL')}
                    </div>
                </div>
            </Router>

        );
    }
}

export default connect(
    (state) => ({ state: state.mainReducer })
)(TeacherItem);