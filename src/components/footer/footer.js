import React, { Component } from 'react';
import logo from '../../assets/img/annaniks_PNG.png';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import '../../assets/css/footer.css';
import { connect } from 'react-redux';



class Footer extends Component {
  render() {
    let store = this.props.state
    return (
      <div className="footer">
        <div className='footer-inside'>
          <img className='footer-logo' src={logo} alt='logo' />
          <div className='footer-links'>
            <div className='col'>

              <Link to="/about_us"><span>{store.link1}</span></Link>
              <a href='#how_work'>{store.link2}</a>
              <a href='#teacher'>{store.link3}</a>
            </div>


          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({ state: state.mainReducer })
)(Footer);
