import React, { Component } from 'react';
import '../../assets/css/registration.css';
import { connect } from 'react-redux';
import Checkboxs from '../checkboxs/checkboxs'

class CheckboxSection extends Component {
    constructor(){
        super();
        this.state={
            show:false
        }
    }
   
    Show(){
        this.setState({show:false})
    }
    render() {
        return (
            <div className='checkbox-section'>
              {this.state.show?<div className='background' onClick={() => { this.Show() }}></div>:null}
                <div className='checkbox-section-heading'>
                    <span>{this.props.heading}</span>
                    <div className='checkbox-round' onClick={()=>{this.setState({show:true})}}>
                    <i className="fa fa-plus" aria-hidden="true"></i>
                    </div>
                </div>
                {this.state.show?<Checkboxs 
                selectAll = {(selectAll)=>{
                    this.props.selectAll(selectAll)
                }}
                show={()=>this.setState({show:true})}
                checkeds = {this.props.checkeds} 
                checkboxName={this.props.checkboxName} 
                onChange={(e)=>{this.props.onChange(e)}} show={()=>this.Show()} list={this.props.list}/>:null}
            </div>
        );
    }
}
export default connect(
    (state) => ({ state: state.mainReducer })
)(CheckboxSection);