import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class StatementItem extends Component {  
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }   
    render() {
        return (
            <div className='StatementItem'>
                <div className='statement-heading'>{this.props.subject_name}</div>
                <div className='statement-text-button'>
                    <div className='statement-text'>{this.props.school_name}</div>
                    <button className='statement-item-button' onClick={() => {this.props.ShowCom()} }>
                    <Link to={`/statement/${this.props.id}`}>
                       {this.props.state.see_more}
                    </Link>
                    </button>
                </div>
            </div>
        );
    }
}
export default connect(
    (state) => ({ state: state.mainReducer }),
)(StatementItem);