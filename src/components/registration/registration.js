import React, { Component } from 'react';
import CheckboxSection from '../checkbox-section/checkbox-section'
import '../../assets/css/registration.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getState, getSubject, fetchInput, getSchools } from '../../api/index'

import TreeView from 'deni-react-treeview';



class Registration extends Component {
	load = false
	validatePhone(phone){
		var re = /^[0-9]/;
		//console.log(re.test(String(phone).toLowerCase()) , phone[0] == '0' , phone.length == 9);
		
		return re.test(String(phone).toLowerCase()) && phone[0] == '0' && phone.length == 9;
	}
	 validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
    constructor(props) {
        super(props);
        this.state = {
			valid:{
				first_name:true,
				second_name:true,
				phone:true,
				email:true
			},
            input: {
                "url": "http://46.101.179.50:9999/teacher/8/",
                "university": "",
                "qualification": "",
                "specialization": "",
                "orientation": "",
                "phone": "",
                "first_name": "",
                "last_name": "",
                "email": "",
                "subject": [],
                "state": [],
                "title":"",
                "accept": false
            },
            show: false,
            selectIndex:0
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }
    componentDidMount() {
        this.props.getState()
        this.props.getSubject()
        this.props.getSchools()
    }

    onChange(e) {
        this.setState({
            input: {
                ...this.state.input,
                [e.target.name]: e.target.value
            }

        })
    }
    schoolsList = []
    componentWillReceiveProps(props) {
        if (!this.load) {
            if (props.state.length > 0 && props.schools.length > 0) {
                this.schoolsList = {id:0,text:"Բոլորը", root:true}
                this.schoolsList.children = props.state.map(item => {
                    return {
                        id: item.url,
                        text: item.name,
                        children: props.schools.filter(schoo => schoo.region == item.url).map(i => ({ id: i.id, text: i.name }))
                    }
                })
                this.load = true
            }


        }
    }

    onSubmit(e) {


        e.preventDefault();

        const input = {
            "url": "http://46.101.179.50:9999/teacher/8/",
            "university": this.state.input.university,
            "specialization": this.state.input.specialization,
            "first_name": this.state.input.first_name,
            "last_name": this.state.input.last_name,
            "email": this.state.input.email,
            "subject": this.state.input.subject,
            "school": this.state.input.state,
            "phone": this.state.input.phone,
		}
		let valid = {}
		valid.first_name = this.state.input.first_name.length>1
		valid.second_name = this.state.input.last_name.length>1
		valid.phone = this.validatePhone(this.state.input.phone)
		valid.email = this.validateEmail(this.state.input.email)
		if(valid.first_name && valid.second_name && valid.phone && valid.email){
			this.props.fetchInput(input);
    		this.props.RegistrationShow()
		}else{
			this.setState({
				valid
			})
		}
       // 
    }
    
    selectIndex(id,list){
        let index=0;
        for(let i=0;i<list.length;i++){
            if(id===list[i].id){
                index=i;
                break;
            } 
        }
       return index 
        
       
    }
    selectSchool(id,list){
        let index=0;
        for(let i=0;i<list.length;i++){
            if(id===list[i].id){
                index=i;
                break;
            } 
        }
       return index 
    }
    render() {     
        const { mainReducer, state,schools, subject } = this.props
        return (
            <div className="registration">

                <div className='registration-inside'>
                    <div className='close'>
                        <i onClick={() => this.props.RegistrationShow()} className="fa fa-remove "></i>
                    </div>
                    <div className='inputs'>
                        <label className='input-item'>
                            <span>{mainReducer.registration_name}</span>
                            <input type='text' name='first_name'
                                value={this.state.input.name}
                                onChange={(e) => this.onChange(e)} 
								placeholder="Պողոս"
								/>
							{!this.state.valid.first_name?<span className="error" >պետք է լինի ամենաքիչը 2 սինվոլ</span>:null}
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_surname}</span>
							<input type='text' name='last_name'
								placeholder= "Պողոսյան"
                                value={this.state.input.surname}
                                onChange={(e) => this.onChange(e)} />
								{!this.state.valid.second_name?<span className="error" >պետք է լինի ամենաքիչը 2 սինվոլ</span>:null}
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_university}</span>
							<input type='text' name='university'
							placeholder = "բուհ-ի անվանում"
                                value={this.state.input.registration_university}
                                onChange={(e) => this.onChange(e)} />
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_profession}</span>
							<input type='text' name='specialization'
								placeholder = "մասնագիտություն"
                                value={this.state.input.registration_profession}
                                onChange={(e) => this.onChange(e)} />
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_phone}</span>
							<input type='text' name='phone'
								placeholder = "xxxxxxxxx"
                                value={this.state.input.registration_profession}
                                onChange={(e) => this.onChange(e)} />
								{!this.state.valid.phone?<span className="error" >պետք է լինի օրինակ xxxxxxxxx</span>:null}
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_email}</span>
							<input type='text' name='email'
								placeholder = "mail@mail.ru"
                                value={this.state.input.registration_profession}
                                onChange={(e) => this.onChange(e)} />
							{!this.state.valid.email?<span className="error" >պետք է լինի օրինակ mail@mail.ru</span>:null}

                        </label>
                        
                        <div className='input-item' style = {{position:"static"}}>
                            <CheckboxSection
                            selectAll = {(selectAll)=>{
                                if(!selectAll){
                                    this.setState((prevState) => {
                                        let index = prevState.input.subject.indexOf(35);
                                        prevState.input.subject = []
                                        if(index>-1)
                                        prevState.input.subject.splice(index,0,35);
                                        return { ...prevState }
                                    })
                                }else{
                                    this.setState((prevState) => {
                                        let index = prevState.input.subject.indexOf(35);
                                        prevState.input.subject = []
                                        prevState.input.subject = subject.filter(item=>item.id!=35).map(item=>item.id);
                                        if(index>-1)
                                        prevState.input.subject.splice(index,0,35);
                                        
                                        return { ...prevState }
                                    })
                                }
                            }}
                            checkboxName='subject' heading={mainReducer.preferred_subjects} list={subject}
                                onChange={(e) => {
                                    let value = e.target.value

                                    this.setState((prevState) => {
                                        let index = prevState.input.subject.indexOf(parseInt(value))
                                        if (index == -1)
                                            prevState.input.subject.push(parseInt(value))
                                        else
                                            prevState.input.subject.splice(index, 1)
                                        return { ...prevState }
                                    })
                                }}
                                checkeds={this.state.input.subject}
                            />
                            <div className='select-item-cont'>
                                {this.state.input.subject.slice(0,this.state.input.subject.length>5?5:this.state.input.subject.length).map((item,index)=><div key={index} className='select-item'>
                                
                                    <div className='blue-round'></div>
                                   <span>{ subject[this.selectIndex(item,subject)].name}</span>

                                </div>)}
                                {
                                this.state.input.subject.length>5?<div className='select-item'><div className='blue-round'></div>
                                <span>...</span></div>:null
                            }
                            </div>
                        </div>
                        <div className='input-item'  style = {{position:"static"}}>
                            <div className='checkbox-section'>
                                {this.state.show ? <div className='background' onClick={() => { this.setState({ show: false }) }}></div> : null}
                                <div className='checkbox-section-heading'>
                                    <span>{mainReducer.community_choice}</span>
                                    <div className='checkbox-round' onClick={() => { this.setState({ show: true }) }}>
                                        <i className="fa fa-plus" aria-hidden="true"></i>
                                    </div>
                                </div>
                                {this.state.show ? <div className='checkox-cont'>
                                    <div className='close'>
                                        <i onClick={() => { this.setState({ show: false }) }} className="fa fa-remove "></i>
                                    </div>
                                    <TreeView showRoot={true}  root = {{text:"Բոլորը"}} items={this.schoolsList} showCheckbox={true} showIcon={false} />
                                    <button className='tree-button' onClick={() => {
                                        let list = []
                                        for (let i = 0; i < this.schoolsList.children.length; i++) {
                                            const element = this.schoolsList.children[i];
                                            list = [...list, ...element.children.filter(item => item.state == 1).map(item => item.id)]
                                        }
                                        this.setState({
                                            input: {
                                                ...this.state.input,
                                                state: list
                                            },
                                            show:false
                                        })

                                    }}>{mainReducer.confirmed}</button>
                                </div> : null}
                            </div>
                            <div className='select-item-cont'>
                            {this.state.input.state.slice(0,this.state.input.state.length>5?5:this.state.input.state.length).map((item,index)=>{return <div key={index} className='select-item'>
                                   <div className='blue-round'></div>
                                    <span>{schools[this.selectSchool(item,schools)].name}</span>
                                    
                            </div>})}
                            {
                                this.state.input.state.length>5?<div className='select-item'><div className='blue-round'></div>
                                <span>...</span></div>:null
                            }
                        </div>
                        </div>
                       
                    </div>
                    <div className='bottom-button'>
                        <button onClick={this.onSubmit}>{mainReducer.send}</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(
    ({ mainReducer, state, subject, schools }) => ({ mainReducer, state, subject, schools }),
    { getState, getSubject, fetchInput, getSchools }
)(Registration);
