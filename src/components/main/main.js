import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../../assets/css/main.css';
import About from '../about/about';
import Teacher from '../teacher/teacher';
import SectionFirst from '../section-first/section-first';



class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }
   componentDidMount(){
    this.props.dispatch({type:'SHOW',payload:false})
}
    render() {
        return (
            <div className="main" id='main'>      
                        <SectionFirst />
                        <About show={() => { this.setState({ show: true }) }} />
                        <Teacher />  
                             
            </div>
        );
    }
}
export default connect(
    (state) => {
    return ({ state: state.mainReducer })
    }
)(Main);