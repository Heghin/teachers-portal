import React, { Component } from 'react';
import arrow1 from "../../assets/img/Path 1.svg";
import arrow2 from "../../assets/img/Path 3.svg";

class Subject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clickVideo: false
    }
  }
  render() {

    return (
      <div className='subject'>
        <div className='subject-round'></div>
        <div className='selected-subject'>Մաթեմատիկա</div>
      </div>
    );
  }
}

export default Subject;