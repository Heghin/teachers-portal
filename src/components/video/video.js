import React, { Component } from 'react';
import arrow1 from "../../assets/img/Path 1.svg";
import arrow2 from "../../assets/img/Path 3.svg";

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clickVideo: false,
      video: [
        "http://teacher.am/1.jpg",
        "http://teacher.am/2.jpg",
      ],
      activeVideo: 0
    }
  }
  render() {

    return (
      <div className='video-section'>
        <div className="video-cont">


          <div className={(!this.state.clickVideo) ? 'video' : 'video largeVideo'} style={{ backgroundImage: "url(" + this.state.video[this.state.activeVideo] + ")" }}>
           
          </div>
          <div className='yellow'>
            <div className='line-arrow'>
              <img className='arrow' src={arrow2}
                onClick={() => {
                  let next = this.state.activeVideo+1;
                  if (next > this.state.video.length ) {
                    next = 0
                  }
                 
                  this.setState({ activeVideo: next })
                }}
              />
              <div className='line'></div>
              <img className='arrow' src={arrow1}
                onClick={() => {
                  let next = this.state.activeVideo;
                  if (next < 1) {
                    next = this.state.video.length - 1
                  }
                  else {
                    next = this.state.activeVideo - 1
                  }
                  this.setState({ activeVideo: next })
                }}
              />
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default Video;