import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {connect} from 'react-redux';
import Top from "./top/top";
import '../../assets/css/header.css';




class Header extends Component {
    constructor(props) {
        super(props);
        this.state={
            show:false
        }
    }

    render() {
        return (
            <Router>
                <div className="Header" >
                <Top/>
                </div>
            </Router>

        );
    }
}

export default connect (
    (state)=>({state:state.mainReducer })
)(Header);