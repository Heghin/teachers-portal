import React, { Component } from 'react';
import icon from '../../../assets/img/icon.png'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux';
import menu from '../../../assets/img/Menu.svg'
import search from '../../../assets/img/Search.svg'
import Registration from "../../registration/registration"





class Top extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            show1: false,
        }
    }
//  <img className='logo' src={icon} />
    render() {
        return (
            <Router>
                <div className="top" >
                <a href="/#main"><div className='logo' style={{backgroundImage:`url(${icon})`}}>
                    
                </div></a>
                  
                    <label className='search-top'>
                        {(this.state.show1) ? <input type="search" name="statement_search" placeholder={this.props.state.search} /> : null}
                        {this.props.mainState.show != '' ? <div className='top-search-icon' onClick={() => { this.setState({ show1: !this.state.show1 }) }}><img src={search} /></div> : null}
                    </label>
                    {!this.state.show ? <img src={menu} onClick={() => this.setState({ show: true })} className='menu' /> :
                        <div className={this.state.show ? 'menu-bar-cont' : 'menu-bar-cont menu-bar-media'}>
                            <div className='menu-bar'>
                                <a className='menu-item' href="/about_us"><span>{this.props.state.navbaritem1}</span></a>
                                <a href='/#how_work' className='menu-item'>{this.props.state.navbaritem2}</a>
                                <a href="http://shirak.mtad.am/structure/info/22/" target="_blank" className='menu-item'>{this.props.state.navbaritem3}</a>
                                <a className='menu-item'>{this.props.state.spaceitem1}</a>
                            </div>
                            <div className='menu-bar-background' onClick={() => this.setState({ show: false })}></div>
                        </div>}
                    <div className='nav-bar'>
                        <a href='/about_us' className='navbar-item'>{this.props.state.navbaritem1}</a>
                        <a href='/#how_work' className='navbar-item'>{this.props.state.navbaritem2}</a>
                        <a href="http://shirak.mtad.am/structure/info/22/" target="_blank" className='navbar-item'>{this.props.state.navbaritem3}</a>
                    </div>
                    <div className='top-right'>
                    <button className='button-header' onClick={() => this.props.dispatch({type:"R_SHOW", payload:true})}><span>{this.props.state.button}</span></button>

                    </div>
                </div>
            </Router>

        );
    }
}

export default connect(
    (state) => ({ state: state.mainReducer, mainState: state.showReducer })
)(Top);