import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux';
import Video from "../video/video";
import Registration from '../registration/registration'
import '../../assets/css/header.css';




class SectionFirst extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }

    render() {
        return (
            <Router>
                <div className="SectionFirst ">
                    <div className='SectionFirst-center-background'>
                        <div className='center'>
                            <div className='SectionFirst-center-heading'>{this.props.state.heading}</div>
                        </div>
                        <div className='opasity'></div>
                    </div>
                   
                    <div className='SectionFirst-top-right-background'></div>
                    <div className='SectionFirst-bottomLeft-background'></div>
                    <Video />
                    
                </div>
            </Router>

        );
    }
}

export default connect(
    (state) => ({ state: state.mainReducer })
)(SectionFirst);