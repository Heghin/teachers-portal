import React, { Component } from 'react';
import validator from "validator";
import { Textbox } from "react-inputs-validation";
import "react-inputs-validation/lib/react-inputs-validation.min.css";
import CheckboxSection from '../checkbox-section/checkbox-section'
import '../../assets/css/registration.css';
import '../../assets/css/teacherAccount.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getState, getSubject, fetchInput, getSchools, login, getMy, getTeacherInfo, saveTeacher } from '../../api/index'
import TreeView from 'deni-react-treeview';

const formValid = ( formErrors, input ) => {
    let valid = true;
    Object.values(formErrors).forEach(element => {if( element.length > 0)  {valid = false} });
    console.log(valid);
    
    Object.values(input).forEach(element => {
        console.log(element,element.length === 0);
        
          if(element.length === 0) {valid = false}
        });
    console.log(valid);
    
    return valid
}

class TeacherAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formErrors: {
                "first_name": "",
                "last_name": "",
                "phone": "",
                "email": "",
            },
            input: {
                "university": "",
           //     "qualification": "",
                "specialization": "",
            //    "orientation": "",
                "phone": "",
                "first_name": "",
                "last_name": "",
                "email": "",
                "subject": [],
                "school": [],
              //  "title": "",
            },
            userData:{},
            show: false,
            selectIndex: 0
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }
    encode(token) {
        let step1 = btoa(token);
        let step2 = btoa("jhfsghrusghth:" + step1 + ":kfgjsaigurgj")
        let step3 = btoa(step2);
        console.log(step3);

        return step3

    }
    componentDidMount() {
        this.props.getState()
        this.props.getSubject()
        this.props.getSchools()
        let p = this.encode("tatevharoyan98@gmail.com|||1111")

        let token = this.props.match.params.token;
        console.log("params", token);

        try {
            let step1 = atob(token);
            let step2 = atob(step1);
            let step3 = step2.substr(14, step2.length - 29);
            console.log("step3", step3);
            let step4 = atob(step3);
            token = step4;
            token = token.split("|||");
            console.log("token", token);

            if (token.length > 1) {
                login(token[0], token[1]).then(res => { 
                    console.log("res", token) 
                    getMy(token).then(data=>{
                        getTeacherInfo(data.id).then(res=>{
                            


                            this.setState({
                                input:{
                                    id:data.id,
                                    university:res.university,
                                    specialization:res.specialization,
                                    phone:res.phone,
                                    user:res.user,
                                    subject:res.subject.map(item=>item.id),
                                    school:res.school.map(item=>item.id),
                                    url:res.url
                                }
                            })
                            
                        })
                    })
                })
            }
        } catch (error) {
            console.log("sxal ka", error);

        }


    }

    onChange(e) {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = this.state.formErrors;
        let b = false;
        switch (name) {
            case "first_name":
                formErrors.first_name =
                    value.length < 3 && value.length > 0 ? "պետք է լինի ամենաքիչը 2 սինվոլ" : "";
                break;
            case "last_name":
                formErrors.last_name =
                    value.length < 3 && value.length > 0 ? "պետք է լինի ամենաքիչը 2 սինվոլ" : "";
                break;
            case "phone":
                b = true
                let re = /^[0-9]/;		
                formErrors.phone =
                !(re.test(String(value).toLowerCase()) && value[0] === '0' && value.length === 9 )? "Պետք է լինի օրինակ xxxxxxxxx" : "";
                break;
            case "email":
                formErrors.email =
                    !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) && value.length > 0 ? "պետք է լինի օրինակ mail@mail.ru" : "";
                break;
            default:
                    b = true
                break;
        }
        if(b){
            this.setState({
                input: {
                    ...this.state.input,
                    [name]: value
                    
                }
    
            })
        }else
        this.setState({
            input: {
                ...this.state.input,
                user:{...this.state.input.user,
                    [name]: value
                }
                
            }

        })
    }
    schoolsList = []

    componentWillReceiveProps(props) {
        if (!this.load) {
            if (props.state.length > 0 && props.schools.length > 0) {
                this.schoolsList = { id: 0, text: "Բոլորը", root: true }
                this.schoolsList.children = props.state.map(item => {
                    return {
                        id: item.id,
                        text: item.name,
                        children: props.schools.filter(schoo => schoo.region == item.url).map(i => ({  state:this.state.input.school.indexOf(i.id)>-1?1:0, id: i.id, text: i.name }))
                    }
                })
                this.schoolsList.children = this.schoolsList.children.map((item)=>{
                    if(item.children.length == item.children.filter(i=>i.state==1).length){
                        item.state = 1;
                    }
                    return item;
                })
                if(this.schoolsList.children.length===this.schoolsList.children.filter(i=>i.state==1).length){
                    this.schoolsList.state = 1;
                }
                this.load = true
            }

        }
    }

    onSubmit(e) {
        e.preventDefault();
        if(formValid(this.state.formErrors,this.state.input)){
            saveTeacher(this.state.input,this.state.input.id).then(data=>{
                console.log(data);
                
            })
        }else{
            console.log("FORM INVALID");
        }
    
        
      
        /* let valid = {}
         valid.first_name = this.state.input.first_name.length > 1
         valid.second_name = this.state.input.last_name.length > 1
         valid.phone = this.validatePhone(this.state.input.phone)
         valid.email = this.validateEmail(this.state.input.email)
         if (valid.first_name && valid.second_name && valid.phone && valid.email) {*/

        /* } else {
             this.setState({
                 valid
             })
         }
         // */
    }

    selectIndex(id, list) {
        let index = 0;
        for (let i = 0; i < list.length; i++) {
            if (id === list[i].id) {
                index = i;
                break;
            }
        }
        return index


    }
    selectSchool(id, list) {
        let index = 0;
        console.log(id);
        
        for (let i = 0; i < list.length; i++) {
            if (id === list[i].id) {
                index = i;
                break;
            }
        }
        return index
    }
    render() {
        const { mainReducer, state, schools, subject } = this.props
        console.log(this.state.input);
        if(!this.load){
            return <div>Loading</div>
        }
        return (
            <div className="teacherAccount">

                <div className='registration-inside'>
                    <div className='token'>{this.props.match.params.token}</div>
                    <div className='inputs'>
                        <label className='input-item'>
                            <span>{mainReducer.registration_name}</span>
                            <input type="text" placeholder="Պողոս" name="first_name" value={this.state.input.user.first_name} onChange={(e) => this.onChange(e)} />
                            <span className='input-invalid'>{this.state.formErrors.first_name}</span>
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_surname}</span>
                            <input type="text" placeholder="Պողոսյան" name="last_name" value={this.state.input.user.last_name} onChange={(e) => this.onChange(e)} />
                            <span className='input-invalid'>{this.state.formErrors.last_name}</span>
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_university}</span>
                            <input type='text' name='university'
                                placeholder="բուհ-ի անվանում"
                                value={this.state.input.university}
                                onChange={(e) => this.onChange(e)} />
                               
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_profession}</span>
                            <input type='text' name='specialization'
                                placeholder="մասնագիտություն"
                                value={this.state.input.specialization}
                                onChange={(e) => this.onChange(e)} />
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_phone}</span>
                            <input type="text" placeholder="xxxxxxxxx" name="phone" value={this.state.input.phone} onChange={(e) => this.onChange(e)} />
                            <span className='input-invalid'>{this.state.formErrors.phone}</span>
                        </label>
                        <label className='input-item'>
                            <span>{mainReducer.registration_email}</span>
                            <input type="email" placeholder="mail@mail.ru" name="email" value={this.state.input.user.email} onChange={(e) => this.onChange(e)} />
                            <span className='input-invalid'>{this.state.formErrors.email}</span>
                        </label>

                        <div className='input-item' style={{ position: "static" }}>
                            <CheckboxSection
                                selectAll={(selectAll) => {
                                    if (!selectAll) {
                                        this.setState((prevState) => {
                                            let index = prevState.input.subject.indexOf(35);
                                            prevState.input.subject = []
                                            if (index > -1)
                                                prevState.input.subject.splice(index, 0, 35);
                                            return { ...prevState }
                                        })
                                    } else {
                                        this.setState((prevState) => {
                                            let index = prevState.input.subject.indexOf(35);
                                            prevState.input.subject = []
                                            prevState.input.subject = subject.filter(item => item.id != 35).map(item => item.id);
                                            if (index > -1)
                                                prevState.input.subject.splice(index, 0, 35);

                                            return { ...prevState }
                                        })
                                    }
                                }}
                                checkboxName='subject' heading={mainReducer.preferred_subjects} list={subject}
                                onChange={(e) => {
                                    let value = e.target.value

                                    this.setState((prevState) => {
                                        let index = prevState.input.subject.indexOf(parseInt(value))
                                        if (index == -1)
                                            prevState.input.subject.push(parseInt(value))
                                        else
                                            prevState.input.subject.splice(index, 1)
                                        return { ...prevState }
                                    })
                                }}
                                checkeds={this.state.input.subject}
                            />
                            <div className='select-item-cont'>
                                {this.state.input.subject.slice(0, this.state.input.subject.length > 5 ? 5 : this.state.input.subject.length).map((item, index) => <div key={index} className='select-item'>

                                    <div className='blue-round'></div>
                                    <span>{subject[this.selectIndex(item, subject)].name}</span>

                                </div>)}
                                {
                                    this.state.input.subject.length > 5 ? <div className='select-item'><div className='blue-round'></div>
                                        <span>...</span></div> : null
                                }
                            </div>
                        </div>
                        <div className='input-item' style={{ position: "static" }}>
                            <div className='checkbox-section'>
                                {this.state.show ? <div className='background' onClick={() => { this.setState({ show: false }) }}></div> : null}
                                <div className='checkbox-section-heading'>
                                    <span>{mainReducer.community_choice}</span>
                                    <div className='checkbox-round' onClick={() => { this.setState({ show: true }) }}>
                                        <i className="fa fa-plus" aria-hidden="true"></i>
                                    </div>
                                </div>
                                {this.state.show ? <div className='checkox-cont'>
                                    <div className='close'>
                                        <i onClick={() => { this.setState({ show: false }) }} className="fa fa-remove "></i>
                                    </div>
                                    <TreeView showRoot={true} root={{ text: "Բոլորը" }} items={this.schoolsList} showCheckbox={true} showIcon={false}   ref="treeview"/>
                                    <button className='tree-button' onClick={() => {
                                        let list = []
                                        for (let i = 0; i < this.schoolsList.children.length; i++) {
                                            const element = this.schoolsList.children[i];
                                            list = [...list, ...element.children.filter(item => item.state == 1).map(item => item.id)]
                                        }
                                        this.setState({
                                            input: {
                                                ...this.state.input,
                                                school: list
                                            },
                                            show: false
                                        })

                                    }}>{mainReducer.confirmed}</button>
                                </div> : null}
                            </div>
                            <div className='select-item-cont'>
                                {schools.length>0?this.state.input.school.slice(0, this.state.input.school.length > 5 ? 5 : this.state.input.school.length).map((item, index) => {
                                    return <div key={index} className='select-item'>
                                        <div className='blue-round'></div>
                                        { <span>{schools[this.selectSchool(item,schools)].name}</span> }

                                    </div>
                                }):null}
                                {
                                    this.state.input.school.length > 5 ? <div className='select-item'><div className='blue-round'></div>
                                        <span>...</span></div> : null
                                }
                            </div>
                        </div>

                    </div>
                    <div className='bottom-button'>
                        <button onClick={this.onSubmit}>{mainReducer.save}</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(
    ({ mainReducer, state, subject, schools }) => ({ mainReducer, state, subject, schools }),
    { getState, getSubject, fetchInput, getSchools }
)(TeacherAccount);
