import React, { Component } from 'react';
import '../../assets/css/registration.css';
import { connect } from 'react-redux';


class Checkboxs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            state: [],
            subjects: [],
            selectAll: false
        }
    }

    render() {
        return (
            <div className='checkox-cont'>
                <div className='close'>
                    <i onClick={() => { this.props.show() }} className="fa fa-remove "></i>
                </div>
                <div className='checkbox-inside'>
                    <div className='teacher-director'>
                        <div className='teacher-director-item'>
                            <span>Ուսուցիչ</span>
                            <div className='yellow-line '></div>
                        </div>
                        <div className='teacher-director-item'>
                            <label className="container">
                                <span className='director'>Տնօրեն</span>
                                <input type="checkbox" checked={this.props.checkeds.indexOf(35) > -1}
                                    onChange={(e) => {
                                        this.props.onChange(
                                            {
                                                target: {
                                                    value: 35
                                                }
                                            }
                                        )
                                    }} />
                                <span className="checkmark"></span>
                            </label>
                            <div className='item-line'></div>
                        </div>
                    </div>
                    <div className='checkboxs'>
                        <label className="container checkboxs-item">
                            <span>Բոլորը</span>
                            <input type="checkbox" checked={this.state.selectAll} onChange={(e) => {
                                let selectAll = !this.state.selectAll;
                                this.setState({
                                    selectAll
                                })

                                this.props.selectAll(selectAll)

                            }} />
                            <span className="checkmark"></span>
                        </label>
                        {this.props.list.filter(item => item.id != 35).map((item, index) => {
                            return <label className="container checkboxs-item" key={index}>
                                <span>{item.name}</span>
                                <input type="checkbox" value={item.id}
                                    name={this.props.checkboxName}
                                    checked={this.props.checkeds.indexOf(item.id) > -1}
                                    onChange={(e) => { this.props.onChange(e) }} />
                                <span className="checkmark"></span>
                            </label>
                        })}
                    </div>
                </div>
                <button className='checkbox-button' onClick={() => this.props.show()} >{this.props.state.confirmed}</button>
            </div>
        );
    }
}
export default connect(
    (state) => ({ state: state.mainReducer })
)(Checkboxs);