import React, { Component } from 'react';

import { connect } from 'react-redux';
import '../../assets/css/conact_with_us.css';



class Contacts extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        let store = this.props.state
        return (<div><div className="logos">
		    <div className="hh_logo"><a className="company_name" href = "http://shirak.mtad.am/" target="_blank">ՀՀ Շիրակի մարզպետարան</a></div>
            <div className="annaniks_logo">
			<a className="company_name" href = "https://www.facebook.com/Annaniks.llc/" target="_blank">«Աննանիկս» ՍՊԸ</a>
			</div>
        </div>
            <div className="Contacts_with_us">
			<p>Նախագիծն իրականացվում է ՀՀ Շիրակի մարզպետարանի և «Աննանիկս» ընկերության համագործակցությամբ՝ նպատակ ունենալով տեղեկատվական աջակցություն ցուցաբերել հանրակրթության ոլորտում աշխատանք փնտրող անձանց։</p>
            </div>
        </div>
        );
    }
}
export default connect(
    (state) => ({ state: state.mainReducer })
)(Contacts);