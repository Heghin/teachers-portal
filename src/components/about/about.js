import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Registration from "../registration/registration"
import { connect } from 'react-redux';
import steps from "../../assets/img/registration.png";
import arrow1 from "../../assets/img/Path 1.svg";
import arrow2 from "../../assets/img/Path 3.svg";



// style={{ backgroundImage: `url(${this.state.images})` }} 

class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeImage: 0,
            show: false,
        }

    }
    handleOnDragStart = e => e.preventDefault();
    render() {
        let store = this.props.state
        return (
            <div className="section" id='how_work'>
                <div className='section-heading'>
                    <span className='heading'>{store.section_about_heading}</span>
                    <div className='section-line'></div>
                </div>
                <div className='section-about'>
                    <div className='about-left'>
                    <button className="about-reagistration" onClick={() => this.props.dispatch({type:"R_SHOW", payload:true})}>{store.button}</button>
                    </div>
                    <div className='about-right'>
                        <div className='about-picture'>
                        <div className="about-image" style = {{backgroundImage:`url(${steps})`}}></div>
                        </div>
                        
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(
    (state) => ({ state: state.mainReducer })
)(About);