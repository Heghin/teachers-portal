import React, { Component } from 'react';
import { connect } from 'react-redux';
import {actionSeeMore,actionAnouncement} from '../../actions/index';
import {getAnnouncement,seeMore,searchShow, getAnnouncementByID} from '../../api';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import SearchLine from '../statement-search/statement-search';
import StatementItem from '../statement-item/statement-item';
import NavBar from '../header/header';
import TextArea from '../textarea/textarea'
import '../../assets/css/main.css';
import '../../assets/css/statement.css'



class Statement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            activId:1
        }
    }
    componentDidMount() {
        this.props.searchShow()
        this.props.getAnnouncement()      
    }
    render() {

        
        return (
            <div className='Statement'>
                <div className='NavBar-SearchLine-cont'>
                    <SearchLine/>
                     <div className='statement-list'>
                        {this.props.announcement.map((item, index) => {                         
                            return <div key={index} onClick={()=>this.setState({activId:item.id})}>
                            <StatementItem id={item.id} ShowCom={() => { this.setState({ show: true }) }}
                                subject_name={item.subject_name}
                                school_name={item.school_name} />
                                </div>
                        })}
                       
                    </div>                   
                    </div>
            </div>
        );
    }
}
export default connect(
    (state) => ({ announcement: state.announcement.filter((item) => item.subject_name.includes(state.findTrack)) }),
    {getAnnouncement,seeMore,searchShow}
    
)(Statement);